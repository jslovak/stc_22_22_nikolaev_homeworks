import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size;
        int countLocMin = 0;
        boolean exist = false;

        //Считываем размер массива, с проверкой на размерность.

        System.out.println("Введите размер массива:");
        do {
            size = scanner.nextInt();
            if (size < 1) {
                System.out.println("Введите корректный размер массива:");
            }
        }
        while (size < 1);

        //Инициализируем массив
        int[] locMin = new int[size];
        System.out.println("Массив из " + size + " элементов инициализирован.");

        //Вводим значения массива через цикл

        for (int i = 0; i < locMin.length; i++) {
            System.out.println("Введите значение " + (i + 1) + " элемента:");
            locMin[i] = scanner.nextInt();
        }
        System.out.println("Массив заполнен следующими значениями:");
        System.out.println(Arrays.toString(locMin));

        //Ищем локальные минимумы заданного массива
        System.out.println("Здесь локальные минимумы:");
        if (locMin[0] < locMin[1]) {
            System.out.print(locMin[0] + " ");
            countLocMin++;
            exist = true;
        }
        for (int i = 1; i < locMin.length - 1; i++) {
            if (locMin[i] < locMin[i - 1] && locMin[i] < locMin[i + 1]) {
                System.out.print(locMin[i] + " ");
                countLocMin++;
                exist = true;
            }
        }
        if (locMin[locMin.length - 1] < locMin[locMin.length - 2]) {
            System.out.print(locMin[locMin.length - 1] + " ");
            countLocMin++;
            exist = true;
        }

        if (exist) {
            System.out.println();
            System.out.println("Следовательно ответ - " + countLocMin + " шт.");
        } else {
            System.out.println("Отсутсвуют");
        }
    }
}