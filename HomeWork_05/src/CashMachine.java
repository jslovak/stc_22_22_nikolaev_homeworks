public class CashMachine {

    // Сумма оставшихся денег в банкомате
    private int cashBalance;

    //Максимальная сумма, разрешенная к выдаче
    private int cashOutMax;

    //Максимальный объем денег (сумма, которая может быть в банкомате)
    private int cashMachineVolume;

    //Количество проведенных операций
    private int numberOfTransactions;

    public CashMachine(int cashBalance, int cashOutMax, int cashMachineVolume, int numberOfTransactions) {
        assert cashMachineVolume >= cashBalance : "Can't create CashMachine with cash balance more then volume";
        this.cashBalance = cashBalance;
        this.cashOutMax = cashOutMax;
        this.cashMachineVolume = cashMachineVolume;
        this.numberOfTransactions = numberOfTransactions;
    }

    // Геттеры и сеттеры для полей банкомата
    public int getCashBalance() {
        return cashBalance;
    }

    public void setCashBalance(int cashBalance) {
        this.cashBalance = cashBalance;
    }

    public int getCashOutMax() {
        return cashOutMax;
    }

    public void setCashOutMax(int cashOutMax) {
        this.cashOutMax = cashOutMax;
    }

    public int getCashMachineVolume() {
        return cashMachineVolume;
    }

    public void setCashMachineVolume(int cashMachineVolume) {
        this.cashMachineVolume = cashMachineVolume;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    //Выдать деньги (не больше, чем разрешено и осталось в банкомате, метод
    //возвращает сумму, которая была в итоге выдана)

    public int getMoney(int money) {
        if (cashBalance >= cashOutMax) {
            if (money <= cashOutMax) {
                cashBalance = cashBalance - money;
                numberOfTransactions++;
                System.out.println("Your amount: " + money + " has been issued");
                return money;
            } else {
                cashBalance = cashBalance - cashOutMax;
                numberOfTransactions++;
                System.out.println("Your amount: " + money + " is more then MaxLimit: " + cashOutMax);
                System.out.println(cashOutMax + " has been issued");
                return cashOutMax;
            }
        } else {
            if (money <= cashBalance) {
                cashBalance = cashBalance - money;
                numberOfTransactions++;
                System.out.println("Your amount: " + money + " has been issued");
                return money;
            } else {
                money = cashBalance;
                cashBalance = 0;
                numberOfTransactions++;
                System.out.println("CashMachine ran out of money");
                System.out.println(money + " has been issued");
                return money;
            }
        }
    }

    //- Положить деньги (если превышен объем, метод должен вернуть сумму, которая не
    //была положена на счет)

    public int setMoney(int money) {
        if (cashMachineVolume >= cashBalance + money) {
            cashBalance = cashBalance + money;
            numberOfTransactions++;
            System.out.println("Your amount: " + money + " has been added");
            return 0;
        } else {
            System.out.println("CashMachine is overloaded");
            System.out.println("Has been added " + (cashMachineVolume - cashBalance));
            money = money + cashBalance - cashMachineVolume;
            cashBalance = cashMachineVolume;
            numberOfTransactions++;
            System.out.println(money + " has been returned");
            return money;
        }
    }
}