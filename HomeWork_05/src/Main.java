public class Main {
    public static void main(String[] args) {

        CashMachine cashMachine = new CashMachine(1500, 1000, 5000, 0);
        assert (cashMachine.getMoney(200) == 200);
        assert (cashMachine.getCashBalance() == 1500 - 200);
        assert (cashMachine.getMoney(500) == 500);
        assert (cashMachine.getMoney(1200) == 800);
        assert (cashMachine.setMoney(1200) == 0);
        assert (cashMachine.getCashBalance() == 1200);
        assert (cashMachine.setMoney(1200) == 0);
        assert (cashMachine.getMoney(1200) == 1000);
        assert (cashMachine.getCashBalance() == 1400);
        assert (cashMachine.setMoney(3700) == 100);
        assert (cashMachine.getNumberOfTransactions() == 7);
    }
}