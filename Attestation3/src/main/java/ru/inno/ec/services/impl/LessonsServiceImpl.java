package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.LessonsService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAll();
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder().name(lesson.getName()).summary(lesson.getSummary()).startTime(lesson.getStartTime()).finishTime(lesson.getFinishTime()).build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public Lesson getLesson(Long id) {
        return lessonsRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm lesson) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();

        lessonForUpdate.setName(lesson.getName());
        lessonForUpdate.setSummary(lesson.getSummary());
        lessonForUpdate.setStartTime(lesson.getStartTime());
        lessonForUpdate.setFinishTime(lesson.getFinishTime());

        lessonsRepository.save(lessonForUpdate);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        lessonsRepository.deleteById(lessonId);
    }
}
