package ru.inno.ec.services;

import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Lesson;

import java.util.List;

public interface LessonsService {

    List<Lesson> getAllLessons();

    void addLesson(LessonForm lesson);

    Lesson getLesson(Long id);

    void updateLesson(Long lessonId, LessonForm lesson);

    void deleteLesson(Long lessonId);
}
