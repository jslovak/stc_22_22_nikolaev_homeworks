public class Main {

    //Функция, возвращающая сумму чисел, в заданном интервале
    public static int sumOfNumberInGetInterval(int from, int to) {

        int sum = 0;
        if (from > to) {
            sum = -1;
        } else {
            for (int i = from; i <= to; i++) {
                sum += i;
            }
        }
        return sum;
    }

    //Процедура вывода четных элементов массива

    public static void printOfEvenNumbers(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                System.out.print(arr[i] + " ");
            }
        }
        System.out.println();
    }

    //Функция перевода массива в число

    public static int toInt(int[] arr) {
        int result = 0;
        int k = 1;
        for (int i = arr.length - 1; i >= 0; i--) {
            result = result + k * arr[i];
            k = k * 10;
        }
        return result;
    }

    public static void main(String[] args) {

        int x = sumOfNumberInGetInterval(2, 10);
        System.out.println(x);

        int[] array = {1, 2, 3, 4, 5, 6, 0, 1, 7};
        printOfEvenNumbers(array);

        int[] number = {9, 8, 7, 2, 3};
        int result = toInt(number);
        System.out.println(result);
    }
}