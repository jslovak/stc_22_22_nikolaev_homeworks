create table driver
(
    id serial primary key,
    first_name char(30),
    last_name char(20),
    phone_number char(12),
    drive_exp integer check (age - drive_exp >= 18) default 0,
    age integer check (age >= 0 and age <= 120) not null,
    have_licence bool,
    licence_category char(5) default '_____',
    rating integer check (rating >= 0 and rating <= 5)
);

create table car (
    id serial primary key,
    model char(20),
    color char(20),
    number_car char(9) unique,
    driver_id integer unique,
    foreign key (driver_id) references driver (id)
);

create table trip (
    id serial primary key,
    driver_id integer,
    car_id integer,
    trip_date timestamp,
    trip_duration time,
    foreign key (driver_id) references driver (id),
    foreign key (car_id) references car (id)
);

