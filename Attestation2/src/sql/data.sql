insert into driver (first_name, last_name, phone_number, drive_exp, age, have_licence, licence_category, rating)
values ('Anton', 'Chijov', +79990003322, 10, 44, true, 'A', 4);
insert into driver (first_name, last_name, phone_number, age, have_licence, rating)
values ('Vera', 'Tepleeva', +79990002233, 30, false, 0);
insert into driver (first_name, last_name, phone_number, drive_exp, age, have_licence, licence_category, rating)
values ('Ivan', 'Abramenko', 89003005500, 15, 40, true, 'ABC', 5);
insert into driver (first_name, last_name, phone_number, drive_exp, age, have_licence, licence_category, rating)
values ('Roman', 'Novoselcev', +89069003311, 13, 33, true, 'ABCDE', 4);
insert into driver (first_name, last_name, phone_number, drive_exp, age, have_licence, licence_category, rating)
values ('Valera', 'Rogov', +79090003421, 10, 28, true, 'ABCDE', 5);

insert into car (model, color, number_car)
values ('Nissan', 'White', 'A121BA121');
insert into car (model, color, number_car, driver_id)
values ('Toyota', 'Black', 'A112BA21', 1);
insert into car (model, color, number_car, driver_id)
values ('Lada', 'Red', 'B121BA121', 2);
insert into car (model, color, number_car, driver_id)
values ('Opel', 'Brown', 'B222BA121', 3);
insert into car (model, color, number_car, driver_id)
values ('Kia', 'White', 'A121BA99', 4);

insert into trip (driver_id, car_id, trip_date, trip_duration)
values (1,5,'2022-12-31', '0:05');
insert into trip (driver_id, car_id, trip_date, trip_duration)
values (3,4,'2021-10-15', '0:10');
insert into trip (driver_id, car_id, trip_date, trip_duration)
values (4,4,'2021-11-24', '1:20');
insert into trip (driver_id, car_id, trip_date, trip_duration)
values (5,1,'2020-01-19', '0:35');
insert into trip (driver_id, car_id, trip_date, trip_duration)
values (3,1,'2021-04-29', '0:51');