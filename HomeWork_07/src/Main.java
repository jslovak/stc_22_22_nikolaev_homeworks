public class Main {

    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }

    public static void main(String[] args) {

        EvenNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(-12, 20);
        OddNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(3, 33);

        Task[] tasks = {evenNumbersPrintTask, oddNumbersPrintTask};
        completeAllTasks(tasks);


    }

}
