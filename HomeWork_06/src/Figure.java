public class Figure {
    private int posX;
    private int posY;

    public Figure(int x, int y) {
        this.posX = x;
        this.posY = y;
    }

    public int posX() {
        return posX;
    }

    public Figure setPosX(int posX) {
        this.posX = posX;
        return this;
    }

    public int posY() {
        return posY;
    }

    public Figure setPosY(int posY) {
        this.posY = posY;
        return this;
    }

    public void moveFigure(int toX, int toY) {
        this.posX = toX;
        this.posY = toY;
    }
}
