public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0, 0, 10, 9);
        Square square = new Square(0, 0, 5);
        Ellipse ellipse = new Ellipse(0, 0, 1, 3);
        Circle circle = new Circle(0, 0, 5);

        rectangle.moveFigure(3, 2);
        assert (rectangle.posX() == 3 && rectangle.posY() == 2);
        assert (rectangle.perimeter() == 38);
        assert (rectangle.area() == 10 * 9);

        square.moveFigure(1, 1);
        assert (square.posX() == 1 && square.posY() == 1);
        assert (square.perimeter() == 5 * 4);
        assert (square.area() == 5 * 5);

        ellipse.moveFigure(5, 5);
        assert (ellipse.posX() == 5 && ellipse.posY() == 5);
        assert (ellipse.perimeter() == 14.050);
        assert (ellipse.area() == 9.425);

        circle.moveFigure(10, 10);
        assert (circle.posX() == 10 && circle.posY() == 10);
        assert (circle.perimeter() == 31.416);
        assert (circle.area() == 78.54);
    }
}
