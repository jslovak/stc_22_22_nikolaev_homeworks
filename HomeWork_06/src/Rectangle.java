public class Rectangle extends Figure {
    private int height;
    private int width;

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public int perimeter() {
        return height * 2 + width * 2;
    }

    public int area() {
        return height * width;
    }
}
