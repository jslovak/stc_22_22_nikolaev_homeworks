import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

public class Ellipse extends Figure {
    private double minRad;
    private double maxRad;

    public Ellipse(int x, int y, double minRad, double maxRad) {
        super(x, y);
        this.minRad = minRad;
        this.maxRad = maxRad;
    }

    public double perimeter() {
        double res = 2 * PI * sqrt((minRad * minRad + maxRad * maxRad) / 2);
        res = Math.ceil(res * 1000);
        return res / 1000;
    }

    public double area() {
        double res = PI * minRad * maxRad;
        res = Math.ceil(res * 1000);
        return res / 1000;
    }
}
