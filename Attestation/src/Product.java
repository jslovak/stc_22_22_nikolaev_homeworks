import java.util.Objects;

public class Product {

    private Integer id;
    private String name;
    private Double price;
    private Integer count;

    public Product(Integer id, String name, Double cost, Integer amount) {
        this.id = id;
        this.name = name;
        this.price = cost;
        this.count = amount;
    }

    public Integer id() {
        return id;
    }

    public String name() {
        return name;
    }

    public Double price() {
        return price;
    }

    public Integer count() {
        return count;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public Product setPrice(Double price) {
        this.price = price;
        return this;
    }

    public Product setCount(Integer count) {
        this.count = count;
        return this;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + price + "|" + count;
    }

}

