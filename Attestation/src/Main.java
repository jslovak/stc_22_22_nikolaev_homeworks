
public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("warehouse.txt");
        assert (productsRepository.findById(7).toString().equals("7|Миндаль|106.8|9"));
        assert (productsRepository.findById(14).toString().equals("14|Зима|100.0|90"));

        System.out.println(productsRepository.findAllByTitleLike("уШ"));
        System.out.println(productsRepository.findAllByTitleLike("ОлО"));
        System.out.println(productsRepository.findAllByTitleLike("ОлОЯЯЯ"));

        /**
         * После первого запуска зима в файле сменится на лето и будет срабатывать assert в 6 строчке )))
         */

        Product winter = productsRepository.findById(14);
        winter.setName("Лето");
        winter.setPrice(200.0);
        winter.setCount(100);
        productsRepository.update(winter);
    }
}
