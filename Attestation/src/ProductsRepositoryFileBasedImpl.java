import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer amount = Integer.parseInt(parts[3]);
        return new Product(id, name, cost, amount);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.id() + "|" + product.name() + "|" + product.price() + "|" + product.count();


    @Override
    public Product findById(Integer id) {
        Product product = null;
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String currentProduct = bufferedReader.readLine();
            while (currentProduct != null) {
                product = stringToProductMapper.apply(currentProduct);
                if(product.id().equals(id)) {
                    return product;
                }
                currentProduct = bufferedReader.readLine();
            }
            System.err.println("Id not found");
            return null;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> products = new ArrayList<>();
        boolean productNotFound = true;
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String currentProduct = bufferedReader.readLine();
            while (currentProduct != null) {
                Product product = stringToProductMapper.apply(currentProduct);
                if(product.name().toLowerCase().contains(title.toLowerCase())) {
                    products.add(product);
                    productNotFound = false;
                }
                currentProduct = bufferedReader.readLine();
            }
            if(productNotFound) {
                System.err.println("Products not found");
                return null;
            }
            return products;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        List<Product> products = new ArrayList<>();
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String currentProduct = bufferedReader.readLine();
            while (currentProduct != null) {
                Product productIn = stringToProductMapper.apply(currentProduct);
                if(productIn.id().equals(product.id())) {
                    productIn.setName(product.name());
                    productIn.setPrice(product.price());
                    productIn.setCount(product.count());
                }
                products.add(productIn);
                currentProduct = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (int i = 0; i < products.size(); i++) {
                String productToSave = productToStringMapper.apply(products.get(i));
                bufferedWriter.write(productToSave);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
