
public class Main {

    public static void main(String[] args) {

        List<String> stringArrayList = new ArrayList<>() {{
            add("Hello!");
            add("Bye!");
            add("Fine!");
            add("C++!");
            add("PHP!");
            add("Cobol!");
        }};

        stringArrayList.printList();
        stringArrayList.remove("Cobol!");
        stringArrayList.printList();
        stringArrayList.removeAt(1);
        stringArrayList.printList();

        List<Integer> intArrayList = new ArrayList<>() {{
            add(11);
            add(22);
            add(33);
            add(44);
            add(55);
            add(66);
        }};
        intArrayList.printList();
        intArrayList.remove(22);
        intArrayList.printList();
        intArrayList.removeAt(0);
        intArrayList.printList();

        /*************LinkedList****************/
        System.out.println("/*************LinkedList****************/");

        List<String> stringLinkedList = new LinkedList<>() {{
            add("Hello!");
            add("Bye!");
            add("Fine!");
            add("C++!");
            add("PHP!");
            add("Cobol!");
        }};

        stringLinkedList.printList();
        stringLinkedList.remove("Fine!");
        stringLinkedList.printList();
        stringLinkedList.removeAt(0);
        stringLinkedList.printList();

        List<Integer> intLinkedList = new LinkedList<>() {{
            add(11);
            add(22);
            add(33);
            add(44);
            add(55);
            add(66);
        }};
        intLinkedList.printList();
        intLinkedList.remove(44);
        intLinkedList.printList();
        intLinkedList.removeAt(4);
        intLinkedList.printList();
    }
}