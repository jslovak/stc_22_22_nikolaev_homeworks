import java.util.HashMap;

public class FindMaxWord {
    public void maxNumberOfWord(String str) {
        String[] words = str.split(" ");
        HashMap<String, Integer> map = new HashMap<>();
        String maxKey = null;
        int maxValue = 0;
        for (String word : words) {
            Integer k = map.get(word);
            if (k != null) {
                map.put(word, k + 1);
                if (k + 1 > maxValue) {
                    maxValue = k + 1;
                    maxKey = word;
                }
            } else {
                map.put(word, 1);
            }
        }
        System.out.println(maxKey + " " + map.get(maxKey));
    }
}
