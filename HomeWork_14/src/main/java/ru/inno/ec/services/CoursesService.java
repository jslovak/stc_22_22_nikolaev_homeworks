package ru.inno.ec.services;

import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;

import java.util.List;

public interface CoursesService {
    void addStudentToCourse(Long courseId, Long studentId);

    void addLessonToCourse(Long courseId, Long lessonId);

    Course getCourse(Long courseId);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);

    List<Course> getAllCourses();

    void addCourse(CourseForm course);

    void deleteCourse(Long courseId);


    List<Lesson> getNotInCourseLessons();

    List<Lesson> getInCourseLessons(Long courseId);

    void deleteLessonFromCourse(Long lessonId);

    void updateCourse(Long courseId, CourseForm course);
}
