package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {

    List<Lesson> findAllByCourseIsNull();

    List<Lesson> findAllByCourseId(Long courseId);
}
