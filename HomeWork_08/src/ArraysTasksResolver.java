public class ArraysTasksResolver {

    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        if(from < 0 || from > array.length || from > to || to > array.length) {
            System.err.println("\n-1\nWrong range");
        } else {
            System.out.print("\n");
            for (int i = from; i <= to; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.print("\n");
            System.out.println(task.resolve(array, from, to));
        }
    }
}
