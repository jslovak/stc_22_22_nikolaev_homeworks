public class Main {
    public static void main(String[] args) {

        ArrayTask arraySumFromTo = (array, from, to) -> {
            if(from < 0 || from > array.length || from > to || to > array.length) {
                System.err.println("Wrong range");
                return -1;
            } else {
                int sum = 0;
                for (int i = from; i <= to; i++) {
                    sum = sum + array[i];
                }
                return sum;
            }
        };

        ArrayTask arraySumNumbersOfMaxValueFromTo = (array, from, to) -> {
            if(from < 0 || from > array.length || from > to || to > array.length) {
                System.err.println("Wrong range");
                return -1;
            } else {
                int max = array[from];
                for (int i = from + 1; i <= to; i++) {
                    if(array[i] > max) {
                        max = array[i];
                    }
                }
                int sum = 0;
                while (max > 0) {
                    sum = sum + max % 10;
                    max = max / 10;
                }
                return sum;
            }
        };

        int[] example = {12, 62, 4, 2, 100, 40, 56};

        ArraysTasksResolver.resolveTask(example, arraySumFromTo, 1, 3);
        ArraysTasksResolver.resolveTask(example, arraySumNumbersOfMaxValueFromTo, 1, 3);

        int[] test = {11, 22, 33, 44, 55,};

        ArraysTasksResolver.resolveTask(test, arraySumFromTo, 2, 4);
        ArraysTasksResolver.resolveTask(test, arraySumNumbersOfMaxValueFromTo, 2, 4);
    }
}