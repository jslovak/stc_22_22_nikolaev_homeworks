package ru.inno.cars.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Car {
    private String number;
    private String model;
    private String color;
    private Integer mileage;
    private Integer cost;
}
