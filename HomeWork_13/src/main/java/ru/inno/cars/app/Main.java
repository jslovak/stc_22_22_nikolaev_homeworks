package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"-action"})
    private List<String> files;

    public static void main(String[] args) {
        Main main = new Main();
        JCommander.newBuilder().addObject(main).build().parse(args);

        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);

        if (main.files.contains("read")) {
            List<Car> cars = carsRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }

        } else if (main.files.contains("write")) {
            Scanner scanner = new Scanner(System.in);
            Car car = new Car();
            int x = 0;
            while (x != -1) {
                System.out.print("Enter number of car: ");
                car.setNumber(scanner.nextLine());
                System.out.print("Enter model of car: ");
                car.setModel(scanner.nextLine());
                System.out.print("Enter color of car: ");
                car.setColor(scanner.nextLine());
                System.out.print("Enter mileage of car: ");
                car.setMileage(scanner.nextInt());
                System.out.print("Enter cost of car: ");
                car.setCost(scanner.nextInt());
                carsRepository.save(car);
                System.out.print("Car added to DB, if you want exit enter -1 or for add new Car enter 1: ");
                x = scanner.nextInt();
                scanner.nextLine();
            }
        } else {
            throw new IllegalArgumentException("You can use only =read or =write parameters");
        }
    }
}