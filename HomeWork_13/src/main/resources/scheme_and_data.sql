drop table car;

create table car
(
    id          serial primary key,
    number      varchar(10),
    model       varchar(20),
    color       varchar(20),
    mileage     integer,
    cost        integer
);

insert into car (number, model, color, mileage, cost)
values ('o001aa111','Camry','Black',120,22000);

insert into car (number, model, color, mileage, cost)
values ('o002aa111','Corolla','Green',100,55000);

insert into car (number, model, color, mileage, cost)
values ('o003aa111','Yaris','Black',133,82000);

insert into car (number, model, color, mileage, cost)
values ('o012aa111','Cami','White',0,72000);

insert into car (number, model, color, mileage, cost)
values ('o705aa111','Bb','Yellow',233,33000);