import java.util.stream.Stream;

public interface CarsRepository {

    String[] getNumberOfCarWithColorOrMileage(String color, int mileage);

    Long getCountOfUnicModelInCostRange(int minCost, int maxCost);

    String getColorOfLowCostCar();

    Double getAverageCostOfModel(String model);

    Stream<Car> findAll();
}
