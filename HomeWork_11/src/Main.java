public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("Garage.txt");

        String[] numbers = carsRepository.getNumberOfCarWithColorOrMileage("Pink", 0);
        for (String number : numbers) {
            System.out.println(number);
        }

        assert (carsRepository.getCountOfUnicModelInCostRange(50000, 100000) == 3);
        assert (carsRepository.getCountOfUnicModelInCostRange(0, 50000) == 2);

        assert (carsRepository.getColorOfLowCostCar().equals("Pink"));

        assert (carsRepository.getAverageCostOfModel("Yaris") == 72000);
        assert (carsRepository.getAverageCostOfModel("Bb") == 32500);
    }
}
