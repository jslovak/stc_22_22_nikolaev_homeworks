import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Stream;

public class CarsRepositoryFileBasedImpl implements CarsRepository {

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static Car stringToCarMapper(String line) {
        String[] parts = line.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer cost = Integer.parseInt(parts[4]);
        return new Car(number, model, color, mileage, cost);
    }

    public Stream<Car> findAll() {
        try {
            return Files.readAllLines(Path.of(fileName))
                    .stream()
                    .map(CarsRepositoryFileBasedImpl::stringToCarMapper);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String[] getNumberOfCarWithColorOrMileage(String color, int mileage) {
        return findAll()
                .filter(car -> color.equals(car.getColor()) || 0 == car.getMileage())
                .map(Car::getNumber).toList()
                .toArray(new String[0]);
    }

    @Override
    public Long getCountOfUnicModelInCostRange(int minCost, int maxCost) {
        return findAll()
                .filter(car -> minCost < car.getCost() && car.getCost() < maxCost)
                .map(Car::getModel)
                .distinct()
                .count();
    }

    @Override
    public String getColorOfLowCostCar() {
        return findAll()
                .sorted(Comparator.comparing(Car::getCost))
                .map(Car::getColor)
                .findFirst().orElse("null");
    }

    @Override
    public Double getAverageCostOfModel(String model) {
        return findAll()
                .filter(car -> car.getModel().equals(model))
                .mapToInt(Car::getCost)
                .average().getAsDouble();
    }

}
